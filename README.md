# activstudio
ActivStudio Discord Bot.

Get now at https://discordbots.org/bot/548132633234767882!

Made with [discord.js,](https://github.com/discordjs/discord.js)
the website of discord.js can be found [here](https://discord.js.org/)

Quick Links:

- Donate: [here](https://www.patreon.com/activstudio)

- Website: [here](https://activstudio.glitch.me/)

- Discord Server: [here](https://discord.gg/XJjSHYB)

- Github Repo (This): [here](https://github.com/catsarecoo/activstudio)

<a href="https://botsfordiscord.com/bots/548132633234767882" >
            <img src="https://botsfordiscord.com/api/bot/548132633234767882/widget" title="Visit ActivStudio listed on Bots for Discord!" alt="ActivStudio's Widget Failed to Load" /></a>
        

[![Build Status](https://travis-ci.com/catsarecoo/activstudio.svg?branch=master)](https://travis-ci.com/catsarecoo/activstudio)

<a href="https://snyk.io/test/github/catsarecoo/Discord-Modmail?targetFile=requirements.txt"><img src="https://snyk.io/test/github/catsarecoo/Discord-Modmail/badge.svg?targetFile=requirements.txt" alt="Known Vulnerabilities" data-canonical-src="https://snyk.io/test/github/catsarecoo/Discord-Modmail?targetFile=requirements.txt" style="max-width:100%;"></a>
